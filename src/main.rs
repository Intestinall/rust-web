extern crate actix_web;
extern crate argonautica;
extern crate chrono;
#[macro_use]
extern crate diesel;
extern crate dotenv;
#[macro_use]
extern crate lazy_static;
extern crate rand;
extern crate regex;
extern crate serde_derive;
#[macro_use]
extern crate serde_json;

use std::env;

use actix_files as fs;
use actix_identity::{CookieIdentityPolicy, IdentityService};
use actix_web::{web, App, HttpResponse, HttpServer};
use chrono::Duration;
use handlebars::Handlebars;

pub mod auth;
mod html;
pub mod json;
pub mod models;
pub mod person;
mod redirect;
pub mod reminder;
pub mod schema;
mod todo;
pub mod traits;
pub mod utils;

// TODO: Include Captcha for registering (see https://www.mtcaptcha.com/pricing)
fn main() -> std::io::Result<()> {
    let cookies_secret_key = env::var("COOKIE_SECRET_KEY")
        .expect("COOKIE_SECRET_KEY must be set")
        .to_string();
    if cookies_secret_key.len() < 32 {
        panic!("COOKIE_SECRET_KEY must be at least 32 characters long")
    }

    HttpServer::new(move || {
        App::new()
            .register_data({
                let mut handlebars = Handlebars::new();
                handlebars
                    .register_templates_directory(".html", "./static/email")
                    .expect("Cannot register handlebars templates");
                web::Data::new(handlebars)
            })
            .wrap(IdentityService::new(
                CookieIdentityPolicy::new(&cookies_secret_key.as_bytes())
                    .name("auth-cookie")
                    .max_age_time(Duration::days(2))
                    .secure(false),
            ))
            // TODO: Add sentry support (see https://actix.rs/docs/sentry/)
            .service(
                fs::Files::new("/css", "static/css")
                    .show_files_listing()
                    .use_last_modified(true),
            )
            .data(web::JsonConfig::default().limit(4096))
            .route("/", web::get().to(html::index))
            .route("/remind_me", web::get().to(reminder::reminder_view::get))
            .service(
                web::scope("/todo")
                    .route("", web::get().to(todo::get_todos))
                    .route("/{id}", web::get().to(todo::remove_todos))
                    .route("/add", web::post().to(todo::add_todos)),
            )
            .service(
                web::scope("/login")
                    .route("", web::get().to(auth::login_view::get))
                    .route("", web::post().to(auth::login_view::post)),
            )
            .service(
                web::scope("/register")
                    .route("", web::get().to(auth::register_view::get))
                    .route("", web::post().to(auth::register_view::post)),
            )
            .route(
                "/confirmation/{token}",
                web::get().to(auth::confirmation_view::get),
            )
            .route("/logout", web::get().to(auth::logout_view::get))
            .service(
                fs::Files::new("/", "static")
                    .show_files_listing()
                    .use_last_modified(true),
            )
            .default_service(web::route().to(|| {
                HttpResponse::NotFound()
                    .content_type("text/html; charset=utf-8")
                    .body(include_str!("../static/html/404.html"))
            }))
    })
    .bind("0.0.0.0:8080")?
    .run()
}
