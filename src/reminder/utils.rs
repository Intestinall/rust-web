use chrono::Utc;
use lettre_email::Email;

use crate::models::Todo;
use crate::reminder::structs::HandlebarMailJson;
use crate::traits::DaysUntilNow;
use crate::utils::send_mail;

pub fn jsonify_todos(todos: Vec<Todo>) -> Vec<HandlebarMailJson> {
    todos
        .into_iter()
        .map(|todo| HandlebarMailJson {
            priority: todo.priority,
            text: todo.text,
            days_since_added: todo.insert_datetime.days_until_now() as i32,
        })
        .collect::<Vec<HandlebarMailJson>>()
}

pub fn send_reminder_mail(mail_content: &str) -> Result<String, String> {
    // TODO : Setup TLS over SMTP (maybe Certbot ?)
    let email = Email::builder()
        .to(("valentinss@protonmail.com", "U"))
        .from("noreply@picatinny.space")
        .subject(format!("Reminder {}", Utc::now().format("%F %H:%M:%S")))
        .html(mail_content)
        .build()
        .unwrap();

    send_mail(email)
}
