use serde_derive::Serialize;

#[derive(Serialize)]
pub struct HandlebarMailJson {
    pub priority: i16,
    pub text: String,
    pub days_since_added: i32,
}
