extern crate lettre;
extern crate lettre_email;

use actix_web::{web, HttpResponse, Responder};
use handlebars::Handlebars;

use crate::models::{Person, Todo};
use crate::reminder::utils::{jsonify_todos, send_reminder_mail};

// TODO : Restrict call to this function to only ip of "cron" docker service
pub fn get(hb: web::Data<Handlebars>) -> impl Responder {
    let mut errors: Vec<String> = Vec::new();

    for person_ in Person::all() {
        let todos: Vec<Todo> = person_.get_todos();

        if !todos.is_empty() {
            let jsonified_todos = jsonify_todos(todos);
            let mail_content = hb.render("reminder", &jsonified_todos).unwrap();

            if let Err(e) = send_reminder_mail(&mail_content) {
                errors.push(e);
            }
        }
    }
    if errors.is_empty() {
        HttpResponse::Ok().body("No problem occurred during the mail send")
    } else {
        HttpResponse::InternalServerError().body(format!(
            "A problem occurred during the mail send\n{}",
            errors.join("\n")
        ))
    }
}
