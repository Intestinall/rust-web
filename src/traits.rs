use chrono::NaiveDateTime;
use chrono::Utc;

pub trait DaysUntilNow {
    fn days_until_now(&self) -> i64;
}

impl DaysUntilNow for NaiveDateTime {
    fn days_until_now(&self) -> i64 {
        Utc::now()
            .naive_utc()
            .signed_duration_since(*self)
            .num_days()
    }
}
