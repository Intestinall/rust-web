use super::schema::{nonconfirmedperson, person, todo};
use crate::utils::establish_connection;
use argonautica::Verifier;
use chrono::NaiveDateTime;
use diesel::prelude::*;
use diesel::ExpressionMethods;
use diesel::Insertable;
use serde_derive::{Deserialize, Serialize};
use std::env;
use uuid::Uuid;

#[derive(Identifiable, Queryable, Serialize, Deserialize)]
#[primary_key(id)]
#[table_name = "person"]
pub struct Person {
    #[serde(skip_deserializing)]
    pub id: i32,
    pub uuid: Uuid,
    pub email: String,
    // TODO: Maybe see for secure string
    pub password: String,
}

#[derive(Identifiable, Queryable, Serialize, Associations, Deserialize, Insertable)]
#[belongs_to(Person)]
#[primary_key(id)]
#[table_name = "todo"]
pub struct Todo {
    #[serde(skip_deserializing)]
    pub id: i32,
    pub text: String,
    pub priority: i16,
    pub insert_datetime: NaiveDateTime,
    pub person_id: i32,
}

#[derive(Identifiable, Queryable, Serialize, Deserialize)]
#[primary_key(id)]
#[table_name = "nonconfirmedperson"]
pub struct NonConfirmedPerson {
    #[serde(skip_deserializing)]
    pub id: i32,
    pub email: String,
    pub password: String,
    pub token: String,
    pub insert_datetime: NaiveDateTime,
}

impl Person {
    pub fn all() -> Vec<Person> {
        use crate::schema::person::dsl::person;
        person
            .load::<Person>(&establish_connection())
            .expect("Error loading persons")
    }

    // TODO: Generalize the above function
    //    pub fn first_from<T>(from_: T, to_match: &str) -> Option<Person>
    //        where T: QueryFragment<Pg>,
    //    {
    //        use crate::schema::person::dsl::*;
    //        match person
    //            .filter(from_.eq(to_match))
    //            .first::<Person>(&establish_connection())
    //        {
    //            Ok(v) => Some(v),
    //            Err(_) => None,
    //        }
    //    }
    pub fn from_email(to_match: &str) -> Option<Person> {
        use crate::schema::person::dsl::{email, person};
        match person
            .filter(email.eq(to_match))
            .first::<Person>(&establish_connection())
        {
            Ok(v) => Some(v),
            Err(_) => None,
        }
    }

    pub fn password_match(&self, password: &str) -> bool {
        Verifier::default()
            .with_hash(&self.password)
            .with_password(password)
            .with_secret_key(env::var("HASH_SECRET_KEY").expect("HASH_SECRET_KEY must be set"))
            .verify()
            .unwrap()
    }

    pub fn get_todos(&self) -> Vec<Todo> {
        use crate::schema::todo::dsl::{insert_datetime, priority};
        Todo::belonging_to(self)
            .order((priority, insert_datetime))
            .load::<Todo>(&establish_connection())
            .expect(&format!(
                "Error loading todos for person with id = {}",
                self.id
            ))
    }
}
