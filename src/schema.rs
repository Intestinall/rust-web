table! {
    board (id) {
        id -> Int4,
        creation_datetime -> Timestamp,
    }
}

table! {
    board_person (id) {
        id -> Int4,
        board_id -> Int4,
        todo_id -> Int4,
        creation_datetime -> Timestamp,
    }
}

table! {
    nonconfirmedperson (id) {
        id -> Int4,
        email -> Varchar,
        password -> Varchar,
        token -> Varchar,
        creation_datetime -> Timestamp,
    }
}

table! {
    person (id) {
        id -> Int4,
        uuid -> Uuid,
        email -> Varchar,
        password -> Varchar,
        creation_datetime -> Timestamp,
    }
}

table! {
    todo (id) {
        id -> Int4,
        text -> Varchar,
        priority -> Int2,
        creation_datetime -> Timestamp,
        board_id -> Int4,
    }
}

joinable!(board_person -> person (board_id));

allow_tables_to_appear_in_same_query!(
    board,
    board_person,
    nonconfirmedperson,
    person,
    todo,
);
