use actix_web::HttpResponse;

use actix_identity::Identity;
use diesel::prelude::*;

use crate::models::Person;
use crate::utils::{establish_connection, redirect_login};
use diesel::PgConnection;
use uuid::Uuid;

pub fn uuid_to_person(uuid_: Uuid) -> Option<Person> {
    use crate::schema::person::dsl::{person, uuid};

    let connection: PgConnection = establish_connection();
    match person.filter(uuid.eq(uuid_)).first::<Person>(&connection) {
        Ok(v) => Some(v),
        Err(_) => None,
    }
}

pub fn identity_to_person(id: &Identity) -> Result<Person, HttpResponse> {
    if let Some(uuid_str) = id.identity() {
        if let Ok(uuid) = Uuid::parse_str(&uuid_str) {
            if let Some(person_) = uuid_to_person(uuid) {
                return Ok(person_);
            }
        }
    }
    Err(redirect_login())
}
