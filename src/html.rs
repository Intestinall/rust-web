use crate::utils::{redirect_login, return_html};
use actix_identity::Identity;
use actix_web::Responder;

pub fn index(id: Identity) -> impl Responder {
    /*
        TODO: Make middleware to avoid code duplication
        See https://github.com/actix/examples/blob/master/middleware/src/redirect.rs#
        and https://github.com/actix/examples/blob/master/middleware/src/main.rs
    */
    if id.identity().is_none() {
        redirect_login()
    } else {
        return_html(include_str!("../static/html/index.html"))
    }
}
