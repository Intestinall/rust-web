use std::env;

use actix_web::{http, HttpResponse};
use chrono::NaiveDateTime;
use chrono::Utc;
use diesel::prelude::*;
use lettre::{ClientSecurity, SmtpClient, Transport};
use lettre_email::Email;
use serde::Serialize;

use actix_web::dev::HttpResponseBuilder;
use dotenv::dotenv;

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url: String = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url).expect(&format!("Error connecting to {}", database_url))
}

pub fn redirect(url: &str) -> HttpResponse {
    HttpResponse::Found()
        .header(http::header::LOCATION, url)
        .finish()
}

pub fn redirect_login() -> HttpResponse {
    redirect("/login")
}

pub fn return_html(html: &'static str) -> HttpResponse {
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(html)
}

pub fn return_json(json: impl Serialize, mut http_code: HttpResponseBuilder) -> HttpResponse {
    match serde_json::to_string(&json) {
        Ok(v) => http_code
            .content_type("application/json; charset=utf-8")
            .body(v),
        Err(_) => HttpResponse::BadRequest().finish(),
    }
}

pub fn send_mail(email: Email) -> Result<String, String> {
    // TODO : Setup TLS over SMTP (maybe Certbot ?)
    let mail_server: &str = &env::var("MAIL_SERVER").expect("MAIL_SERVER must be set");
    let mut mailer = SmtpClient::new((mail_server, 25), ClientSecurity::None)
        .unwrap()
        .transport();

    match mailer.send(email.into()) {
        Ok(_) => Ok("Email sent".to_string()),
        Err(e) => Err(format!("Could not send email: {}\n", e)),
    }
}

pub fn days_since_naive_datetime(naive_datetime: NaiveDateTime) -> i64 {
    Utc::now()
        .naive_utc()
        .signed_duration_since(naive_datetime)
        .num_days()
}
