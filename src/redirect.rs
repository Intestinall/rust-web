use actix_identity::Identity;
use actix_service::{Service, Transform};
use actix_web::dev::{ServiceRequest, ServiceResponse};
use actix_web::{http, Error, FromRequest, HttpResponse};
use futures::future::{ok, Either, FutureResult};
use futures::Poll;

#[allow(dead_code)]
pub struct CheckLogin;

impl<S, B> Transform<S> for CheckLogin
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type InitError = ();
    type Transform = CheckLoginMiddleware<S>;
    type Future = FutureResult<Self::Transform, Self::InitError>;

    fn new_transform(&self, service: S) -> Self::Future {
        ok(CheckLoginMiddleware { service })
    }
}

#[derive(Debug)]
pub struct CheckLoginMiddleware<S> {
    service: S,
}

impl<S, B> Service for CheckLoginMiddleware<S>
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Future = Either<S::Future, FutureResult<Self::Response, Self::Error>>;

    fn poll_ready(&mut self) -> Poll<(), Self::Error> {
        self.service.poll_ready()
    }

    fn call(&mut self, req: ServiceRequest) -> Self::Future {
        // We only need to hook into the `start` for this middleware.

        let uuid: String;
        let path = req.path().to_string();
        let (reqq, mut payload) = req.into_parts();

        {
            uuid = Identity::from_request(&reqq, &mut payload)
                .unwrap()
                .identity()
                .unwrap_or("".to_string());
            //            uuid = Identity::extract(&reqq).unwrap().identity().unwrap_or("".to_string());
        }

        if uuid != "" {
            println!("SALUT : {}", uuid);
            let sr = ServiceRequest::from_parts(reqq, payload);
            let srr = sr.ok().unwrap();
            Either::A(self.service.call(srr))
        } else {
            if &path == "/login" {
                println!("Vous allez vers /login");
                let sr = ServiceRequest::from_parts(reqq, payload);
                let srr = sr.ok().unwrap();
                Either::A(self.service.call(srr))
            } else {
                println!("NO AUTH : vous allez etre redirigé vers /login");
                let sr = ServiceResponse::new(
                    reqq,
                    HttpResponse::Found()
                        .header(http::header::LOCATION, "/login")
                        .finish()
                        .into_body(),
                );
                Either::B(ok(sr))
            }
        }
    }
}
