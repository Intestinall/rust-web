use crate::json::AddTodo;
use crate::models::Todo;
use crate::person::identity_to_person;
use crate::utils::establish_connection;
use actix_identity::Identity;
use actix_web::{web, HttpResponse, Responder};
use diesel::prelude::*;

pub fn get_todos(id: Identity) -> impl Responder {
    let person_ = match identity_to_person(&id) {
        Ok(v) => v,
        Err(redirection) => return redirection,
    };

    // TODO: Add "since X days" in payload
    let string_json: String = match serde_json::to_string(&person_.get_todos()) {
        Ok(v) => v,
        Err(_) => return HttpResponse::Forbidden().finish(),
    };

    HttpResponse::Ok()
        .content_type("application/json")
        .body(string_json)
}

pub fn remove_todos(id: Identity, id_to_remove: web::Path<i32>) -> impl Responder {
    use crate::schema::todo::dsl::id as todo_id;

    let person_ = match identity_to_person(&id) {
        Ok(v) => v,
        Err(redirection) => return redirection,
    };

    let connection: PgConnection = establish_connection();

    match diesel::delete(Todo::belonging_to(&person_).filter(todo_id.eq(id_to_remove.into_inner())))
        .execute(&connection)
    {
        Ok(_) => HttpResponse::NoContent().finish(),
        Err(_) => HttpResponse::InternalServerError().finish(),
    }
}

pub fn add_todos(id: Identity, todo_to_add: web::Json<AddTodo>) -> impl Responder {
    use crate::schema::todo::dsl::{person_id, priority, text, todo};

    let person_ = match identity_to_person(&id) {
        Ok(v) => v,
        Err(redirection) => return redirection,
    };

    let connection: PgConnection = establish_connection();

    match diesel::insert_into(todo)
        .values((
            text.eq(&todo_to_add.text),
            priority.eq(todo_to_add.priority),
            person_id.eq(person_.id),
        ))
        .get_result(&connection)
    {
        Ok(v) => {
            let v: Todo = v;
            HttpResponse::Ok()
                .content_type("application/json")
                .body(serde_json::to_string(&v.id).unwrap())
        }
        Err(_) => HttpResponse::InternalServerError().finish(),
    }
}
