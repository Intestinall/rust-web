use serde_derive::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct AddTodo {
    pub text: String,
    pub priority: i16,
}
