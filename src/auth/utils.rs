use argonautica::Hasher;
use std::env;

pub fn hash_password(password: &str) -> String {
    // TODO: make secret key global static var
    Hasher::default()
        .with_password(password)
        .with_secret_key(env::var("HASH_SECRET_KEY").expect("HASH_SECRET_KEY must be set"))
        .hash()
        .unwrap()
}
