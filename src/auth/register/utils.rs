use actix_web::web;
use diesel::prelude::*;
use diesel::{insert_into, PgConnection};
use lettre_email::Email;
use rand::{distributions, Rng};
use regex::Regex;

use crate::auth::register::structs::RegisterPostJson;
use crate::auth::utils::hash_password;
use crate::models::{NonConfirmedPerson, Person};
use crate::utils::send_mail;
use handlebars::Handlebars;

pub fn email_is_valid(email: &str) -> bool {
    lazy_static! {
        static ref MAIL_REGEX: Regex = Regex::new(r".+@.+\..+").unwrap();
    }
    MAIL_REGEX.is_match(email)
}

pub fn generate_url_token() -> String {
    rand::thread_rng()
        .sample_iter(distributions::Alphanumeric)
        .take(50)
        .collect()
}

pub fn person_exists(email_input: &str, connection: &PgConnection) -> bool {
    use crate::schema::person::dsl::{email, person};
    person
        .filter(email.eq(email_input))
        .first::<Person>(connection)
        .is_ok()
}

pub fn non_confirmed_person_exists(email_input: &str, connection: &PgConnection) -> bool {
    use crate::schema::nonconfirmedperson::dsl::{email, nonconfirmedperson};
    nonconfirmedperson
        .filter(email.eq(email_input))
        .first::<NonConfirmedPerson>(connection)
        .is_ok()
}

pub fn insert_non_confirmed_person(
    params: &web::Json<RegisterPostJson>,
    url_token: &str,
    connection: &PgConnection,
) {
    use crate::schema::nonconfirmedperson::dsl::{email, nonconfirmedperson, password, token};

    let ppp = hash_password(&params.password);
    insert_into(nonconfirmedperson)
        .values((
            email.eq(&params.email),
            password.eq(&ppp),
            token.eq(url_token),
        ))
        .execute(connection)
        .unwrap();
}

pub fn send_confirmation_email(hb: web::Data<Handlebars>, email: &str, url_token: &str) {
    let html_email = hb
        .render("confirmation", &json!({ "url_token": url_token }))
        .unwrap(); // TODO: Kill every .unwrap()

    let email = Email::builder()
        .to((email, email))
        .from("noreply@picatinny.space")
        .subject("Confirmation for Todo List inscription")
        .html(html_email)
        .build()
        .unwrap();

    let abc = send_mail(email);
    if abc.is_ok() {
        println!("Mail send !");
    } else {
        println!("Mail NOT NOT NOT send.")
    }
}
