use serde_derive::{Deserialize, Serialize};

#[derive(Deserialize, Debug)]
pub struct RegisterPostJson {
    pub email: String,
    pub password: String,
}

#[derive(Serialize, Debug)]
pub struct RegisterPostJsonResponse {
    pub message: String,
}
