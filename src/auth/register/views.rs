use actix_web::{web, HttpResponse, Responder};
use diesel::PgConnection;

use crate::auth::register::structs::{RegisterPostJson, RegisterPostJsonResponse};
use crate::auth::register::utils::{
    email_is_valid, generate_url_token, insert_non_confirmed_person, non_confirmed_person_exists,
    person_exists, send_confirmation_email,
};
use crate::utils::{establish_connection, return_json};
use handlebars::Handlebars;

pub fn post(hb: web::Data<Handlebars>, params: web::Json<RegisterPostJson>) -> impl Responder {
    if !email_is_valid(&params.email) {
        return return_json(
            RegisterPostJsonResponse {
                message: "Invalid email format".to_string(),
            },
            HttpResponse::BadRequest(),
        );
    }

    let connection: PgConnection = establish_connection();

    if !person_exists(&params.email, &connection)
        && !non_confirmed_person_exists(&params.email, &connection)
    {
        let url_token = generate_url_token();

        insert_non_confirmed_person(&params, &url_token, &connection);
        send_confirmation_email(hb, &params.email, &url_token);
        return_json(
            RegisterPostJsonResponse {
                message: format!(
                    "A confirmation email will soon be sent to \"{}\"",
                    &params.email
                ),
            },
            HttpResponse::Ok(),
        )
    } else {
        return_json(
            RegisterPostJsonResponse {
                message: format!("User with email \"{}\" already exists", &params.email),
            },
            HttpResponse::Conflict(),
        )
    }
}

pub fn get() -> impl Responder {
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(include_str!("../../../static/html/auth/register.html"))
}
