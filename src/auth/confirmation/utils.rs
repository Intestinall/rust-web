use crate::models::NonConfirmedPerson;
use diesel::insert_into;
use diesel::prelude::*;
use uuid::Uuid;

pub fn token_to_non_confirmed_user(
    token_url: &str,
    connection: &PgConnection,
) -> Option<NonConfirmedPerson> {
    use crate::schema::nonconfirmedperson::dsl::{nonconfirmedperson, token};

    match nonconfirmedperson
        .filter(token.eq(token_url))
        .first::<NonConfirmedPerson>(connection)
    {
        Ok(v) => Some(v),
        Err(_) => None,
    }
}

pub fn insert_person(params: &NonConfirmedPerson, connection: &PgConnection) {
    use crate::schema::person::dsl::{email, password, person, uuid};

    insert_into(person)
        .values((
            email.eq(&params.email),
            password.eq(&params.password),
            uuid.eq(Uuid::new_v4()),
        ))
        .execute(connection)
        .unwrap();
}
