use actix_web::{web, Responder};
use diesel::prelude::*;
use diesel::PgConnection;

use crate::auth::confirmation::utils::{insert_person, token_to_non_confirmed_user};
use crate::utils::{establish_connection, redirect_login, return_html};

pub fn get(token: web::Path<String>) -> impl Responder {
    let connection: PgConnection = establish_connection();

    if let Some(non_confirmed_person) = token_to_non_confirmed_user(&token, &connection) {
        insert_person(&non_confirmed_person, &connection);
        diesel::delete(&non_confirmed_person)
            .execute(&connection)
            .unwrap();
        redirect_login()
    } else {
        return_html(include_str!("../../../static/html/auth/confirmation.html"))
    }
}
