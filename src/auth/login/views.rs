use actix_web::{web, HttpResponse, Responder};

use crate::auth::login::structs::{LoginPostJson, LoginPostJsonResponse};
use crate::models::Person;
use crate::utils::{redirect, return_html, return_json};
use actix_identity::Identity;

pub fn post(id: Identity, params: web::Json<LoginPostJson>) -> impl Responder {
    if let Some(person_) = Person::from_email(&params.email) {
        if person_.password_match(&params.password) {
            id.remember(person_.uuid.to_string());
            return redirect("/");
        } else {
            return return_json(
                LoginPostJsonResponse {
                    message: format!("User \"{}\" password doesn't match", &params.email),
                },
                HttpResponse::Unauthorized(),
            );
        }
    } else {
        return return_json(
            LoginPostJsonResponse {
                message: format!("No user found with email \"{}\"", &params.email),
            },
            HttpResponse::NotFound(),
        );
    }
}

pub fn get(id: Identity) -> impl Responder {
    if let Some(_uuid) = id.identity() {
        redirect("/")
    } else {
        return_html(include_str!("../../../static/html/auth/login.html"))
    }
}
