use serde_derive::{Deserialize, Serialize};

#[derive(Deserialize, Debug)]
pub struct LoginPostJson {
    pub email: String,
    pub password: String,
}

#[derive(Serialize, Debug)]
pub struct LoginPostJsonResponse {
    pub message: String,
}
