pub mod confirmation;
pub mod login;
pub mod logout;
pub mod register;
mod utils;

pub use crate::auth::confirmation::views as confirmation_view;
pub use crate::auth::login::views as login_view;
pub use crate::auth::logout::views as logout_view;
pub use crate::auth::register::views as register_view;
