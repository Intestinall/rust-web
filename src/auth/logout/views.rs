use actix_web::Responder;

use crate::utils::redirect_login;
use actix_identity::Identity;

pub fn get(id: Identity) -> impl Responder {
    id.forget();
    redirect_login()
}
