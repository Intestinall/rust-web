temp_postgresql:
	docker run --name rust_postgres -e POSTGRES_PASSWORD=mypass -p 5432:5432 --rm postgres:11-alpine

temp_mail:
	docker run -it -p 25:25 --rm my_postfix:latest

filldb:
	docker exec -i rust_postgres psql -U postgres postgres < dump.sql
