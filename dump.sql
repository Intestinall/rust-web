BEGIN;
INSERT INTO person (id, uuid, email, password) VALUES
   (1, '99b0e7b3-918e-42a2-b6b8-2a211d2e6864', 'aaa@aaa.aaa', '$argon2id$v=19$m=4096,t=192,p=8$A7R1ltuSfZTRYgHg/Vl+tQSxQZ5UVkkZYc724EJkW9E$glF3CZc1vcendGUigjSymmzqiDQI2/v/ABHicYtuOik'),
   (3, '99b0e7b3-918e-42a2-b6b8-555555555555', 'bbb@bbb.bbb', '$argon2id$v=19$m=4096,t=192,p=8$f6rfn4KJ74tfeV0Lu5GnDoH9nANWG4pC85TPlqVndAM$nzr9KG6MKwylKiHWiheNO/8x7S9nuIIRW1+dxE9ljQ8'),
   (4, '99b0e7b3-918e-42a2-b6b8-111111111111', 'ccc@ccc.ccc', '$argon2id$v=19$m=4096,t=192,p=8$HrtiHofGCZqDQN6y+RTzCeybcs8EbP5ajcWuZMn9Yto$oGzRic7aOP+LMgO8I4g2CGw7s8UPLFYldXKw67UehfM');

INSERT INTO todo (id, text, priority, insert_datetime, person_id) VALUES
    (3, 'Yo yo yo DAOIDNAOZIDN', 4, '2019-11-04 10:44:15.390242', 1),
    (6, 'CCCCC', 0, '2019-07-04 10:44:15.39', 1),
    (4, 'AAAAA', 2, '2019-11-01 10:44:15.39', 1),
    (5, 'BBBBB', 1, '2019-10-04 10:44:15.39', 1),
    (10, 'DDDDD', 1, '2019-10-04 10:44:15.39', 3),
    (7, 'EEEEE', 1, '2019-10-04 10:44:15.39', 3),
    (8, 'FFFFF', 1, '2019-10-04 10:44:15.39', 4),
    (9, 'GGGGG', 1, '2019-10-04 10:44:15.39', 4);
COMMIT;