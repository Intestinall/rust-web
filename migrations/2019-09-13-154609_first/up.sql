-- Your SQL goes here
CREATE TABLE nonconfirmedperson (
    id SERIAL PRIMARY KEY,
    email VARCHAR NOT NULL UNIQUE,
    password VARCHAR NOT NULL,
    token VARCHAR NOT NULL UNIQUE,
    creation_datetime TIMESTAMP NOT NULL DEFAULT now()
);
CREATE INDEX nonconfirmedperson_email_index ON nonconfirmedperson USING BTREE (email);
CREATE INDEX nonconfirmedperson_token_index ON nonconfirmedperson USING BTREE (token);

----------------------------------------------------------------------------------------

CREATE TABLE person (
    id SERIAL PRIMARY KEY, --<--------------------------------------------------------------------------------------------------------
    uuid UUID NOT NULL UNIQUE,                                                                                                      --\
    email VARCHAR NOT NULL UNIQUE,                                                                                                  --|
    password VARCHAR NOT NULL,                                                                                                      --|
    creation_datetime TIMESTAMP NOT NULL DEFAULT now()                                                                              --|
);                                                                                                                                  --|
CREATE INDEX person_uuid_index ON person USING BTREE (uuid);                                                                        --|
CREATE INDEX person_email_index ON person USING BTREE (email);                                                                      --|
                                                                                                                                    --|
----------------------------------------------------------------------------------------                                            --|
                                                                                                                                    --|
CREATE TABLE board (                                                                                                                --|
    id SERIAL PRIMARY KEY, --<--------------------------------------------------------------------------------------------------    --|
    name VARCHAR NOT NULL,                                                                                                    --\   --|
    creation_datetime TIMESTAMP NOT NULL DEFAULT now()                                                                        --\   --|
);                                                                                                                            --|   --|
                                                                                                                              --|   --|
----------------------------------------------------------------------------------------                                      --|   --|
                                                                                                                              --|   --|
CREATE TABLE board_person (                                                                                                   --|   --|
    id SERIAL PRIMARY KEY,                                                                                                    --|   --|
    board_id INTEGER NOT NULL, CONSTRAINT fk_board_person FOREIGN KEY (board_id) REFERENCES person (id) ON DELETE CASCADE, -->--/   --|
    todo_id INTEGER NOT NULL, CONSTRAINT fk_todo_board FOREIGN KEY (todo_id) REFERENCES board (id) ON DELETE CASCADE, -->-------------|
    creation_datetime TIMESTAMP NOT NULL DEFAULT now()
);

----------------------------------------------------------------------------------------

CREATE TABLE todo (
    id SERIAL PRIMARY KEY,
    text VARCHAR NOT NULL,
    priority SMALLINT NOT NULL CHECK (0 <= priority AND priority <= 5),
    creation_datetime TIMESTAMP NOT NULL DEFAULT now(),
    board_id INTEGER NOT NULL, CONSTRAINT fk_todo_board FOREIGN KEY (board_id) REFERENCES board (id) ON DELETE CASCADE
);
